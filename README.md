# Faderstart

Arduino script to start next song when a fader is pulled up.

## Idea

The Arduino is registered as a keyboard at the playout computer. The playout software is configured to start the next song when spacebar is pressed. When the fader is pulled up, a signal is send to the Arduino on sensor A1 and the Arduino then "presses" spacebar, thus starting the next song.

## Hardware

We use an arduino leonardo for its keyboard-emulation functionality.
