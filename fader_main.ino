#include <Keyboard.h>

int sensor = A1;
const int threshold = 512;

bool isFaderUp() {
  return analogRead(sensor) > threshold;
}

bool isFaderDown() {
  return not isFaderUp();
}

// the setup function runs once when you press reset or power the board
void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Keyboard.begin();
}

// the loop function runs over and over again forever
void loop() {
  waitForPullDown();
  waitForPullUp();
  Keyboard.print(" ");
  //Keyboard.print("_space_");    debug
}


/*
 * Returns once the fader values is up for 5 consecutive reads, 2ms apart (10ms total)
 */
void waitForPullUp() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on
  for (int counter = 0; counter < 5; counter++){
    if ( isFaderDown() ) counter = 0; // reset loop, if the fader is still down
    //Keyboard.print("_up_");    debug
    delay(2); // wait 2ms
  }
}


/*
 * Returns once the fader values is low for 5 consecutive reads, 4ms apart (20ms total)
 */
void waitForPullDown() {
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off
  for (int counter = 0; counter < 5; counter++){
    if ( isFaderUp() ) counter = 0; // reset loop, if the fader is still up
    // Keyboard.print("_down_");    debug
    delay(4); // wait 4ms
  }
}
